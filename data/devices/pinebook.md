---
name: "Pinebook"
deviceType: "laptop"
portType: "Native"
maturity: .25
buyLink: "https://www.pine64.org/pinebook/"
installLink: "https://ci.ubports.com/job/rootfs/job/rootfs-pinebook/"
description: "The Pinebook is an affordable ARM-based linux laptop. An experimental Ubuntu Touch image is available."
---
